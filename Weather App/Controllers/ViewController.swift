//
//  ViewController.swift
//  Weather App
//
//  Created by muxammed on 08.09.2020.
//  Copyright © 2020 peydalyzatlar. All rights reserved.
//

import UIKit
import Nuke

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    
    var goroda:[Gorod] = []
    var filteredGoroda: [Gorod] = []
    let cellId = "cellId"
    let searchController = UISearchController(searchResultsController: nil)
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }
    
    let gorodaCollection:UICollectionView = {
        
        let layoutManager = UICollectionViewFlowLayout()
        var cv = UICollectionView(frame: .zero,collectionViewLayout: layoutManager)
        cv.backgroundColor = .white
        return cv
    }()
    
    let progress:UIActivityIndicatorView = {
        var pg = UIActivityIndicatorView(style: .large)
        pg.hidesWhenStopped = true
        pg.style = .large
        pg.color = .lightGray
        pg.translatesAutoresizingMaskIntoConstraints = false
        return pg
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .green
        // Do any additional setup after loading the view.
        
        setupViews()
        setupDummyData()
        
        
    }
    
    
    func setupDummyData(){
        var dummyGorod = Gorod(id: 1, name: "Абакан", lan: 53.720976, lon: 91.442423)
        self.goroda.append(dummyGorod)
        dummyGorod = Gorod(id: 2, name: "Архангельск", lan: 64.539304, lon: 40.518735)
        self.goroda.append(dummyGorod)
        dummyGorod = Gorod(id: 3, name: "Астрахань", lan: 46.347869, lon: 48.033574)
        self.goroda.append(dummyGorod)
        dummyGorod = Gorod(id: 4, name: "Барнаул", lan: 53.356132, lon: 83.749619)
        self.goroda.append(dummyGorod)
        dummyGorod = Gorod(id: 5, name: "Белгород", lan: 50.597467, lon: 36.588849)
        self.goroda.append(dummyGorod)
        dummyGorod = Gorod(id: 6, name: "Бийск", lan: 52.541444, lon: 85.219686)
        self.goroda.append(dummyGorod)
        dummyGorod = Gorod(id: 7, name: "Благовещенск", lan: 50.290658, lon: 127.527173)
        self.goroda.append(dummyGorod)
        dummyGorod = Gorod(id: 8, name: "Братск", lan: 56.151382, lon: 101.634152)
        self.goroda.append(dummyGorod)
        dummyGorod = Gorod(id: 9, name: "Брянск", lan: 53.2434, lon: 34.364198)
        self.goroda.append(dummyGorod)
        dummyGorod = Gorod(id: 10, name: "Великий Новгород", lan: 58.521475, lon: 31.275475)
        self.goroda.append(dummyGorod)
        self.filteredGoroda = self.goroda
        
        self.progress.startAnimating()
        for gorod in self.goroda {
            loadGorodData(lat: gorod.lan, lon: gorod.lon)
        }
    }
    
    func setupViews(){
        
        self.view.addSubview(gorodaCollection)
        
        self.view.addConsWithFormat(format: "H:|[v0]|", views: gorodaCollection)
        self.view.addConsWithFormat(format: "V:|[v0]|", views: gorodaCollection)
        
        self.gorodaCollection.dataSource = self
        self.gorodaCollection.delegate = self
        self.gorodaCollection.register(GorodCell.self, forCellWithReuseIdentifier: cellId)
        
        //get data from yandex API
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Поиск городов"
        
        
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        
        self.gorodaCollection.addSubview(progress)
        self.view.addConstraint(NSLayoutConstraint.init(item: progress, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint.init(item: progress, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
    }
    
    @objc func gotoDetails(){
        let dtvc = DetailedWeatherController()
        self.navigationController?.pushViewController(dtvc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if isFiltering {
          return filteredGoroda.count
        }
          
        return goroda.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! GorodCell
        let gorod: Gorod
        if isFiltering {
          gorod = filteredGoroda[indexPath.row]
        } else {
          gorod = goroda[indexPath.row]
        }
        
        let svgURL = URL(string: "https://yastatic.net/weather/i/icons/blueye/color/svg/\(gorod.weatherData?.fact.icon ?? "bkn_d").svg")!
        cell.iconView.downloadedsvg(from: svgURL)
        cell.gorodName.text = gorod.name
        cell.gorodFactTemperature.text = NSString(format:"\(gorod.weatherData?.fact.temp ?? 0)%@" as NSString, "\u{00B0}") as String
//        //cell.gorodFactTemperature.text = "\(gorod.weatherData?.fact.temp ?? 0)"
//
//        let temp:Int = gorod.weatherData?.fact.temp ?? 0
//        if (temp <= 10){
//            cell.gradusView.image!.overlayImage(color: .blue)
////            cell.gradusView.tintColor = .blue
//            print("HANY")
//        } else if (temp > 20) {
//            cell.gradusView.image!.overlayImage(color: .red)
//        } else {
//            cell.gradusView.image!.overlayImage(color: .black)
//        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (progress.isHidden){
            let dtvc = DetailedWeatherController()
            if (isFiltering){
                dtvc.selectedGorod = self.filteredGoroda[indexPath.item]
            } else {
                dtvc.selectedGorod = self.goroda[indexPath.item]
            }
            self.navigationController?.pushViewController(dtvc, animated: true)
        } else {
            print("its loading ")
        }
        
        
    }

    func filterContentForSearchText(_ searchText: String,
                                    goroda: [Gorod]? = nil) {
        filteredGoroda = self.goroda.filter { (gorod: Gorod) -> Bool in
        return gorod.name.lowercased().contains(searchText.lowercased())
      }
      
        self.gorodaCollection.reloadData()
    }
    
    func loadGorodData(lat:Double,lon:Double){
        
        let postString = "lat=\(lat)&lon=\(lon)";
        let Url = String(format: "https://api.weather.yandex.ru/v2/forecast?\(postString)")
        guard let serviceUrl = URL(string: Url) else { return }
        var request = URLRequest(url: serviceUrl)
        request.setValue("6ca34e29-9fc6-4362-9120-c21ab6243c57", forHTTPHeaderField: "X-Yandex-API-Key")
        request.httpMethod = "GET"
        let session = URLSession.shared
        
        //request.httpBody = postString.data(using: String.Encoding.utf8);
        session.dataTask(with: request) { (data, response, error) in
            
            if ((error) != nil){
                DispatchQueue.main.async {
                    self.progress.stopAnimating()
                    print("GATA \(error)")
                    
                }
                return
            }
            
            if let data = data {
                do {
                    
                    let cevap = try JSONDecoder().decode(GorodData.self, from: data)
                    if let row = self.goroda.firstIndex(where: { $0.lan == cevap.info.lat && $0.lon == cevap.info.lon}) {
                        self.goroda[row].weatherData = cevap
                        
                    }
                    
                    
                    let gainedGoroda  = self.goroda.filter({ $0.weatherData?.info.lat == nil})
                    if (gainedGoroda.count == 0){
                        print("reload it")
                        
                        DispatchQueue.main.async {
                            self.progress.stopAnimating()
                            self.gorodaCollection.reloadData()
                        }
                        
                    }
                    
                } catch {
                    print(error)
                    self.progress.stopAnimating()
                    
                }
            }
        }.resume()
        
    }

}

struct Gorod {
    var id:Int
    var name:String
    var lan:Double
    var lon:Double
    var weatherData:GorodData?
}




extension ViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    let searchBar = searchController.searchBar
    filterContentForSearchText(searchBar.text!)
  }
}

struct GorodData: Codable {
    
    let info: Info
    let fact: Fact
    //let forecasts: [Forecast]

    
}

struct Info: Codable {
    
    let lat, lon: Double
    
}

struct Fact: Codable {
    let temp, feelsLike: Int
    let icon: String
    let condition: Condition
    let windSpeed: Double
    let windGust:Double
    let humidity:Double
    let windDir: WindDir
    

    enum CodingKeys: String, CodingKey {
        case temp,icon, condition,humidity
        case feelsLike = "feels_like"
        case windSpeed = "wind_speed"
        case windGust = "wind_gust"
        case windDir = "wind_dir"
        
    }
}


enum WindDir : String, Codable {
    case nw = "nw"
    case n = "n"
    case ne = "ne"
    case e = "e"
    case se = "se"
    case s = "s"
    case sw = "sw"
    case w = "w"
    case с = "c"
}


enum Condition: String, Codable {
    case clear = "clear"
    case partlyCloudy = "partly-cloudy"
    case cloudy = "cloudy"
    case overcast = "overcast"
    case drizzle = "drizzle"
    case lightRain = "light-rain"
    case rain = "rain"
    case moderateRain = "moderate-rain"
    case heavyRain = "heavy-rain"
    case continuousHeavyRain = "continuous-heavy-rain"
    case showers = "showers"
    case wetSnow = "wet-snow"
    case lightSnow = "light-snow"
    case snow = "snow"
    case snowShowers = "snow-showers"
    case hail = "hail"
    case thunderstorm = "thunderstorm"
    case thunderstormWithRain = "thunderstorm-with-rain"
    case thunderstormWithHail = "thunderstorm-with-hail"
    
}




