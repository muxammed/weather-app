//
//  DetailedWeatherController.swift
//  Weather App
//
//  Created by muxammed on 08.09.2020.
//  Copyright © 2020 peydalyzatlar. All rights reserved.
//

import UIKit

class DetailedWeatherController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    var selectedGorod:Gorod?
    var details:[Details] = []
    
    let gorodName:UILabel = {
        var lb = UILabel()
        lb.font = UIFont.systemFont(ofSize: 40, weight: .bold)
        return lb
    }()
    
    let gradusView:UIImageView = {
        var im = UIImageView()
        im.contentMode = UIImageView.ContentMode.scaleAspectFit
        return im
    }()
    
    let detailsCollection:UICollectionView = {
        
        let layoutManager = UICollectionViewFlowLayout()
        var cv = UICollectionView(frame: .zero,collectionViewLayout: layoutManager)
        cv.backgroundColor = .white
        return cv
    }()
    
    
    
    let topView = UIView()
    let bottomView = UIView()
    let seperatorView = UIView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationItem.title = selectedGorod?.name
        setupViews()
    }
    
    func setupViews(){
        self.view.backgroundColor = .white
        
        self.view.addSubview(topView)
        self.view.addSubview(seperatorView)
        self.view.addSubview(bottomView)
        
        self.topView.addSubview(gradusView)
        self.topView.addSubview(gorodName)
        
        self.bottomView.addSubview(detailsCollection)
        self.detailsCollection.dataSource = self
        self.detailsCollection.delegate = self
        self.detailsCollection.register(DetailCell.self, forCellWithReuseIdentifier: "cellId")
        
        self.seperatorView.backgroundColor = .darkGray
        
        self.view.addConsWithFormat(format: "H:|[v0]|", views: topView)
        self.view.addConsWithFormat(format: "H:|[v0]|", views: seperatorView)
        self.view.addConsWithFormat(format: "H:|[v0]|", views: bottomView)
        self.view.addConsWithFormat(format: "V:|[v0][v1(==0.5)][v2(==v0)]|", views: topView,seperatorView,bottomView)
        self.topView.addConsWithFormat(format: "H:|-30-[v0]-30-|", views: gradusView)
        self.topView.addConsWithFormat(format: "V:|-70-[v0]|", views: gradusView)
        
        self.topView.addConsWithFormat(format: "H:|-30-[v0]|", views: gorodName)
        self.topView.addConsWithFormat(format: "V:[v0]-20-|", views: gorodName)
        
        self.bottomView.addConsWithFormat(format: "H:|[v0]|", views: detailsCollection)
        self.bottomView.addConsWithFormat(format: "V:|[v0]|", views: detailsCollection)
        
        
        self.gorodName.alpha = 0
        self.gorodName.text = selectedGorod?.name
        self.gorodName.fadeIn()
        
        let svgURL = URL(string: "https://yastatic.net/weather/i/icons/blueye/color/svg/\(selectedGorod?.weatherData?.fact.icon ?? "bkn_d").svg")!
        gradusView.downloadedsvg(from: svgURL)
        
        generateDetails()
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return details.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! DetailCell
        cell.itemNameLabel.text = details[indexPath.item].itemname
        cell.itemValueLabel.text = details[indexPath.item].value
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 50)
    }
    
    
    func generateDetails(){
        var detail = Details(itemname: "Температура (°C)", value: "\(selectedGorod?.weatherData?.fact.temp ?? 0)")
        details.append(detail)
        detail = Details(itemname: "Ощущаемая температура (°C)", value: "\(selectedGorod?.weatherData?.fact.feelsLike ?? 0)")
        details.append(detail)
        detail = Details(itemname: "Погода", value: getCondition((selectedGorod?.weatherData?.fact.condition)!))
        details.append(detail)
        detail = Details(itemname: "Скорость ветра (в м/с).", value: "\(selectedGorod?.weatherData?.fact.windSpeed ?? 0)")
        details.append(detail)
        detail = Details(itemname: "Скорость порывов ветра (в м/с).", value: "\(selectedGorod?.weatherData?.fact.windGust ?? 0)")
        details.append(detail)
        detail = Details(itemname: "Направление ветра", value: getWindDirection((selectedGorod?.weatherData?.fact.windDir)!))
        details.append(detail)
        detail = Details(itemname: "Влажность воздуха (в процентах)", value: "\(selectedGorod?.weatherData?.fact.humidity ?? 0)")
        details.append(detail)
        
        self.detailsCollection.reloadData()
    }
    

    func getWindDirection(_ direction:WindDir)->String{
        switch direction {
        case .nw:
            return "северо-западное"
        case .n :
            return "северное"
        case .ne :
            return "северо-восточное"
        case .e :
            return "восточное"
        case .se :
            return "юго-восточное"
        case .s :
            return "южное"
        case .sw :
            return "юго-западное"
        case .w :
            return "западное"
        case .с :
            return "штиль"
        }
    }
    
    func getCondition(_ condition:Condition)->String{
        switch condition {
        case .clear:
            return "ясно"
        case .partlyCloudy:
            return "малооблачно"
        case .cloudy:
            return "облачно с прояснениями"
        case .overcast:
            return "пасмурно"
        case .drizzle:
            return "морось"
        case .lightRain:
            return "небольшой дождь"
        case .rain:
            return "дождь"
        case .moderateRain:
            return "умеренно сильный дождь"
        case .heavyRain:
            return "сильный дождь"
        case .continuousHeavyRain:
            return "длительный сильный дождь"
        case .showers:
            return "ливень"
        case .wetSnow:
            return "дождь со снегом"
        case .lightSnow:
            return "небольшой снег"
        case .snow:
            return "снег"
        case .snowShowers:
            return "снегопад"
        case .hail:
            return "град"
        case .thunderstorm:
            return "гроза"
        case .thunderstormWithRain:
            return "дождь с грозой"
        case .thunderstormWithHail:
            return "гроза с градом"
        }
            
    }

}




struct Details {
    let itemname:String
    let value:String
}

