//
//  GorodCell.swift
//  Weather App
//
//  Created by muxammed on 17.09.2020.
//  Copyright © 2020 peydalyzatlar. All rights reserved.
//

import UIKit


class DetailCell:UICollectionViewCell{
    
    let itemNameLabel:UILabel = {
        var lb = UILabel()
        lb.font = UIFont.systemFont(ofSize: 18, weight: .thin)
        lb.numberOfLines = 2
        return lb
    }()

    let itemValueLabel:UILabel = {
        var lb = UILabel()
        lb.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        lb.textAlignment = .right
        lb.numberOfLines = 2
        return lb
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    let seperatorView = UIView()


    func setupViews(){
        self.addSubview(itemNameLabel)
        self.addSubview(itemValueLabel)
        self.addSubview(seperatorView)
        self.seperatorView.backgroundColor = .white

        self.addConsWithFormat(format: "H:|-15-[v0][v1(==v0)]-15-|", views: itemNameLabel,itemValueLabel)
        self.addConsWithFormat(format: "V:[v0(==0.5)]|", views: seperatorView)
        self.addConsWithFormat(format: "H:|-20-[v0]-20-|", views: seperatorView)
        self.addConstraint(NSLayoutConstraint.init(item: itemValueLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint.init(item: itemNameLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
    
    
}
