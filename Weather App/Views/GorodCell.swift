//
//  GorodCell.swift
//  Weather App
//
//  Created by muxammed on 17.09.2020.
//  Copyright © 2020 peydalyzatlar. All rights reserved.
//

import UIKit


class GorodCell:UICollectionViewCell{
    
    let gorodName:UILabel = {
        var lb = UILabel()
        lb.font = UIFont.systemFont(ofSize: 25, weight: .thin)
        return lb
    }()
    
    let gorodFactTemperature:UILabel = {
        var lb = UILabel()
        lb.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        return lb
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let iconView : UIImageView = {
        let im = UIImageView()
        im.contentMode = UIImageView.ContentMode.scaleAspectFill
        return im
    }()
    
    let gradusView : UIImageView = {
        let im = UIImageView()
        
        im.contentMode = UIImageView.ContentMode.scaleAspectFit
        im.image = UIImage(named: "gradusnik")
        im.image?.withRenderingMode(.alwaysTemplate)
        return im
    }()
    
    let seperatorView = UIView()
    
    
    func setupViews(){
        self.addSubview(gorodName)
        self.addSubview(iconView)
        self.addSubview(gorodFactTemperature)
        self.addSubview(gradusView)
        self.addSubview(seperatorView)
        self.seperatorView.backgroundColor = .blue
        
        self.addConsWithFormat(format: "H:|-15-[v0]-8-[v1(==50)]-10-[v2]-8-[v3(==70)]-15-|", views: gorodName,gradusView,gorodFactTemperature,iconView)
        self.addConsWithFormat(format: "V:[v0(==70)]", views: iconView)
        self.addConsWithFormat(format: "V:[v0(==30)]", views: gradusView)
        self.addConsWithFormat(format: "V:[v0(==0.5)]|", views: seperatorView)
        self.addConsWithFormat(format: "H:|-20-[v0]-20-|", views: seperatorView)
        self.addConstraint(NSLayoutConstraint.init(item: iconView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint.init(item: gradusView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint.init(item: gorodFactTemperature, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint.init(item: gorodName, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    
}
