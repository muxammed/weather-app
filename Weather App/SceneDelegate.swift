//
//  SceneDelegate.swift
//  Weather App
//
//  Created by muxammed on 08.09.2020.
//  Copyright © 2020 peydalyzatlar. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: UIScreen.main.bounds)
        let viewController = ViewController()
        let nav1 = UINavigationController(rootViewController: viewController)
        window?.rootViewController = nav1
        window?.makeKeyAndVisible()
        window?.windowScene = windowScene
    }

    

}

